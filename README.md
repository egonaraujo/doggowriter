Doggo Writer
====

What is it?
---

Doggo Writer is a game created in [Godot Engine](https://godotengine.org/).
The development start on March of 2019, but only got a real push and effort
near the end of April 2019.

The game is about a dog that wants to be famous in his dog village.
He finds that one way to do this is to become a writer of adventures.
Unfortunately, dogs don't go in adventures and he has no ideas for his book.
So he goes to a human village to write his histories,
and that's where the game begins.

It's a puzzle + collectible kind of game, very similar to the
[Grow series from eyemaze](http://www.eyezmaze.com/).
You control the dog that indirectly controls the adventurers, and you have
to collect all possible histories you can get from guiding them.
But beware, a wrong order of histories will lead to an unfullfilled book.

Gameplay
---

Use arrows or WASD to move and click or press E on objects to interact

Compiling + running the game
---

Doggo Writer was developed with Godot Engine 3.1 stable version on a
Debian system.
We believe that the way the files work in Godot should be independent on
OS, but if there is any problem on non-Linux systems we are not really aware.

To run it simply open Godot 3.1 or older and select "Open Existing Project",
and then select this folder.

In a latter date, we will have a build/ directory with HTML5, Linux and Windows
builds (Apple builds require an Apple-owned system, one which we don't have at
the moment)

Credits
---

- Egon Nathan Bittencourt Araujo - Game Designer, Game Developer
- Daniela Jagher - Art Designer

Thanks
---

- Lucas Fernandes de Oliveira - For all the help and insights about the whole
game development pipeline


