extends Node2D

var selectedAdv = null
var lastPosPlayer

func _ready():
	lastPosPlayer = $YSort/Player.position

func _process(delta):
	$Camera2D.position = $YSort/Player.position
	
	if (lastPosPlayer.distance_to($YSort/Player.position) > 32 && selectedAdv != null):
		lastPosPlayer = $YSort/Player.position
		if(selectedAdv.isFollowing == true):
			var moves = Array()
			moves = call_a_star(selectedAdv,$YSort/Player, lastPosPlayer)
			if (moves.size() > 0):
				selectedAdv.moveTo(moves)
			else:
				print("Didn't find a way from " \
					+ str(selectedAdv.position) \
					+ " to " \
					+ str(lastPosPlayer)\
					)

func _Player_selectedAdventurer(adventurer):
	selectedAdv = adventurer
	selectedAdv.isFollowing = true


func call_a_star(movingObj, player, targetPos):
	var posArray = []
	var directions = a_star_non_rec(movingObj,player,targetPos)
	if(directions.size() > 0 && directions.has(targetPos)):
		var pos = targetPos
		while (pos != movingObj.position) :
			posArray.push_front(pos)
			pos = directions[pos]
		return posArray
	else:
		return []

func a_star_non_rec(movingObj,dog, targetPos):
	var frontier = [movingObj.position]
	var cameFrom = {movingObj.position:Vector2(0,0)}
	
	var physicsShape = Physics2DShapeQueryParameters.new()
	physicsShape.set_collide_with_areas(false)
	physicsShape.set_collide_with_bodies(true)
	physicsShape.set_exclude([movingObj,dog])
	var shape = RectangleShape2D.new()
	shape.set_extents(Vector2(32,32))
	
	physicsShape.set_shape(shape)
	
	while (!frontier.empty()):
		var coord = frontier.pop_front()
		if (    coord.x > 1024 || coord.x < 0 \
			||  coord.y > 1024 || coord.y < 0):
				continue
		var posUp    = coord + Vector2(0,-16)
		var posDown  = coord + Vector2(0,+16)
		var posLeft  = coord + Vector2(-16,0)
		var posRight = coord + Vector2(16,0)
		var neighbors = [posUp,posDown,posLeft,posRight]
		for point in neighbors:
			physicsShape.set_transform(Transform2D(Vector2(1,0),Vector2(0,1),point))
			var space_state = get_world_2d().direct_space_state
			var result = space_state.intersect_shape(physicsShape)
			if(result.size() > 0):
				continue
			if (!cameFrom.has(point)):
				frontier.push_back(point)
				cameFrom[point]=coord
			if (point.distance_to(targetPos) < 15):
				if(point!=targetPos):
					cameFrom[targetPos]=point
				frontier.clear()
				break
	return cameFrom
