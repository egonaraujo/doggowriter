extends KinematicBody2D

signal selectAdventurer(adventurer, player)

export (float) var speed = 50.0
var velocity = Vector2()
var actualAnim = ""

var advInArea = Array()
var targetInArea 

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	var animation = "Idle"
	velocity.x = 0
	velocity.y = 0

	if Input.is_action_pressed("ui_up"):
		animation = "MoveUp"
		velocity.y = -1
	if Input.is_action_pressed("ui_down"):
		animation = "MoveDown"
		velocity.y = 1
	if Input.is_action_pressed("ui_left"):
		animation = "MoveLeft"
		velocity.x = -1
	if Input.is_action_pressed("ui_right"):
		animation = "MoveRight"
		velocity.x = 1
	if Input.is_action_just_pressed("ui_interact"):
		if(!advInArea.empty()):
			emit_signal("selectAdventurer",advInArea.front())
		else:
			print("no adventurer on list")
	
	if(actualAnim != animation):
		$AnimationPlayer.play(animation)
	
	actualAnim = animation
	
	move_and_slide(velocity.normalized()*speed)

func _on_Area2D_body_entered(body):
	if(body.get_name().find("Adventurer")!= -1):
		advInArea.push_back(body)
		if(!body.is_connected("isClicked",self, "advClicked")):
			body.connect("isClicked",self,"advClicked")
	if(advInArea.size()==1):
		var interactButton = load("res://InteractButton/InteractButton.tscn").instance()
		advInArea.front().add_child(interactButton)


func _on_Area2D_body_exited(body):
	var isFirst = false if advInArea.empty() else advInArea.front() == body
	if(body.get_name().find("Adventurer")!= -1 \
	&& advInArea.has(body)):
		if( isFirst ):
			var interactButton = body.get_node("InteractButton")
			interactButton.queue_free()
		advInArea.erase(body)
		if(body.is_connected("isClicked",self, "advClicked")):
			body.disconnect("isClicked",self,"advClicked")
	if(isFirst && advInArea.size()>0):
		var interactButton = load("res://InteractButton/InteractButton.tscn").instance()
		advInArea.front().add_child(interactButton)

func advClicked(adv):
	if(advInArea.find(adv) > -1):
		emit_signal("selectAdventurer", adv,self)
	