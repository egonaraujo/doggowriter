extends KinematicBody2D


signal isClicked(adv)

var strength = 0
var empathy = 0
var trust = 0

export (float) var speed = 50.0
var actualAnim = ""

var isFollowing = false
var listPos = Array()
var newPos = position
var isActive = true
var raycasts

func _ready():
	newPos = position

func _physics_process(delta):
	var animation = ""
	var velocity = Vector2(0.0,0.0)

	animation = ""
	if position.y-1 > newPos.y:
		animation = "MoveUp"
		velocity.y = -1
	if position.y+1 < newPos.y:
		animation = "MoveDown"
		velocity.y = 1
	if position.x-1 > newPos.x:
		animation = "MoveLeft"
		velocity.x = -1
	if position.x+1 < newPos.x:
		animation = "MoveRight"
		velocity.x = 1
	
	if(animation == ""):
		if (listPos.size() > 0):
			newPos = listPos.pop_front()
		else:
			position = newPos
			newPos = position
		$AnimationPlayer.stop(false)
	elif(actualAnim != animation):
		$AnimationPlayer.play(animation)
		
	if(listPos.size()>0):
		raycasts[0].set_cast_to((listPos.front() + Vector2(-4,6)*scale) - (raycasts[0].global_position))
		raycasts[1].set_cast_to((listPos.front() + Vector2(0,6)*scale) - (raycasts[1].global_position))
		raycasts[2].set_cast_to((listPos.front() + Vector2(4,6)*scale) - (raycasts[2].global_position))
		raycasts[0].force_raycast_update()
		raycasts[1].force_raycast_update()
		raycasts[2].force_raycast_update()
		if(    !raycasts[0].is_colliding() \
			&& !raycasts[1].is_colliding() \
			&& !raycasts[2].is_colliding() \
			):
			newPos = listPos.pop_front()
	elif(!isActive && newPos == position):
		isActive=true
		for r in raycasts :
			remove_child(r)
			r.queue_free()
	
	actualAnim = animation
	
	move_and_slide(velocity.normalized()*speed)


func moveTo(listMoves):
	if(isActive):
		raycasts = [RayCast2D.new(),RayCast2D.new(),RayCast2D.new()]
		raycasts[0].set_name("RayCastL")
		raycasts[1].set_name("RayCastM")
		raycasts[2].set_name("RayCastR")
		raycasts[0].position += Vector2(-4,6) 
		raycasts[1].position += Vector2(0,6)
		raycasts[2].position += Vector2(4,6)
		add_child(raycasts[0])
		add_child(raycasts[1])
		add_child(raycasts[2])
		listPos = listMoves
		isActive = false
	else:
		listPos = listMoves

func set_status(v_str, v_emp, v_tru):
	strength = v_str
	empathy = v_emp
	trust = v_tru

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	&& event.button_index == BUTTON_LEFT \
	&& event.is_pressed():
		emit_signal("isClicked",self)

func _on_Area2D_area_entered(area):
	if(area.get_name().find("Target")!= -1):
		#get position of area
		isFollowing=false
